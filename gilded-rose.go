package main

type Item struct {
	name            string
	sellIn, quality int
}

type Actualizador interface {
	updateQuality()
}

type Normal struct {
	item Item
}

func (n *Normal) updateQuality() {
	if n.item.quality > 0 {
		n.item.quality--
	}
	n.item.sellIn--
	applyCommonRestrictions(&n.item.sellIn, &n.item.quality)
}

type AgedBrie struct {
	item Item
}

func (a *AgedBrie) updateQuality() {
	a.item.quality++
	a.item.sellIn--

	if a.item.sellIn < 0 {
		a.item.quality++ // Increase 2 units
	}
	applyCommonRestrictions(&a.item.sellIn, &a.item.quality)
}

type Sulfuras struct {
	item Item
}

func (s *Sulfuras) updateQuality() {
	// nothing
}

type Backstage struct {
	item Item
}

func (b *Backstage) updateQuality() {
	b.item.sellIn--
	b.item.quality++
	switch {
	case b.item.sellIn < 10: // less than 10 days
		b.item.quality++
		fallthrough
	case b.item.sellIn < 5: // less than 5 days
		b.item.quality++
		fallthrough
	case b.item.sellIn < 0 : // expired
		b.item.quality = 0
	}
	applyCommonRestrictions(&b.item.sellIn, &b.item.quality)
}

type Conjured struct {
	item Item
}

func (c *Conjured) updateQuality() {
	c.item.sellIn--
	if c.item.quality > 0 {
		c.item.quality -= 2
	}
	applyCommonRestrictions(&c.item.sellIn, &c.item.quality)
}

func applyCommonRestrictions(sellIn *int, quality *int) {
	if *quality > 50 {
		*quality = 50
	}

	if *quality < 0 {
		*quality = 0
	}
}

func UpdateQualityAll(items []Actualizador) {
	for _, item := range items {
		item.updateQuality()
	}
}
