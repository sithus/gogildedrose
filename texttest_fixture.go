package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	fmt.Println("OMGHAI!")

	var items = []Actualizador{
		&Normal{item: Item{"+5 Dexterity Vest", 10, 20}},
		&AgedBrie{item: Item{"Aged Brie", 2, 0}},
		&Normal{item: Item{"Elixir of the Mongoose", 5, 7}},
		&Sulfuras{item: Item{"Sulfuras, Hand of Ragnaros", 0, 80}},
		&Sulfuras{item: Item{"Sulfuras, Hand of Ragnaros", -1, 80}},
		&Backstage{item: Item{"Backstage passes to a TAFKAL80ETC concert", 15, 20}},
		&Backstage{item: Item{"Backstage passes to a TAFKAL80ETC concert", 10, 49}},
		&Backstage{item: Item{"Backstage passes to a TAFKAL80ETC concert", 5, 49}},
		&Conjured{item: Item{"Conjured Mana Cake", 3, 6}}, // <-- :O
	}

	days := 2
	var err error
	if len(os.Args) > 1 {
		days, err = strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		days++
	}

	for day := 0; day < days; day++ {
		fmt.Printf("-------- day %d --------\n", day)
		fmt.Println("name, sellIn, quality")
		for i := 0; i < len(items); i++ {
			fmt.Println(items[i])
		}
		fmt.Println("")
		UpdateQualityAll(items)
	}
}
